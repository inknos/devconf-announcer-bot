generate-schedule-sched.py uses sched.com api to generate schedule_for_bot.json, which is in the following format:
```json
[
    {
        "title": "Welcome!",
        "start": "2023-06-16T09:30:00+02:00",
        "room": "d105"
    },
]
```

send_messages.py then creates simple Q&A threads in the existing rooms with matching names.

```bash
podman pull registry.gitlab.com/inknos/devconf-announcer-bot
```

```bash
podman run --rm -d --name devconf-bot \
    -e HOMESERVER=<your homeserver> \
    -e USERNAME=<your bot username> \
    -e PASSWORD=<your bot password> \
    -e DEVICE_ID=<your bot device id> \
    -v $(pwd)/schedule_for_bot.json:/app/schedule_for_bot.json:z \
    devconf-bot:latest 
```