import asyncio
import json
from datetime import datetime, timedelta
from os import getenv
from pathlib import Path
from zoneinfo import ZoneInfo  # python3.8+
from nio import AsyncClient, LocalProtocolError, RoomResolveAliasError

import logging

logging.basicConfig(format='[%(levelname)s] %(asctime)s %(message)s', filename="log/bot.log", level=logging.DEBUG)

async def calculate_delay(start) -> float:
    # Parse schedule data
    start_time = datetime.fromisoformat(start)
    current_time = datetime.now(tz=ZoneInfo(tz))
    return (start_time - current_time).total_seconds()


async def send_messages(title: str, room: str, start_time, client):
    """Send a message thread in the room and pin it"""
    # skip talk that are scheduled for not today
    if datetime.fromisoformat(start_time).date() != datetime.today().date():
        logging.debug(f"Talk not today: {room} - {title}")
        return
    delay = await calculate_delay(start_time)
    if delay < 0:
        logging.debug(f"Event in past: {room} - {title}")
        return
    if delay > 0:
        # Wait until scheduled start time
        logging.debug(f"Planned: {room} - {title} at {datetime.fromisoformat(start_time).time()} - {delay}s delay from the start")
        await asyncio.sleep(delay)
    try:
        room_addr = f"#{room}:{homeserver}"
        response = await client.room_resolve_alias(room_addr)
        room_id = response.room_id
        content = {
            "body": f"### 🙋Q&A Thread - {title}",
            "msgtype": "m.text",
            "format": "org.matrix.custom.html",
            "formatted_body": f"<h3>🙋Q&amp;A Thread - {title}</h3>\n",
        }
        root = await client.room_send(room_id, "m.room.message", content)
        # Sending reply
        content = {
            "m.relates_to": {"rel_type": "m.thread", "event_id": root.event_id},
            "msgtype": "m.text",
            "body": "📢 Ask your questions to the speaker(s) and/or upvote existing ones here.",
            "is_falling_back": True,
            "m.in_reply_to": {"event_id": root.event_id},
        }
        await client.room_send(room_id, "m.room.message", content)
        logging.debug(f"Sent: {room} - {title}")
        content = {"pinned": [room_id]}
        # Pinning
        pinned = []
        if not replace_pinned:
            room_state_pinned = await client.room_get_state_event(
                room_id, event_type="m.room.pinned_events"
            )
            try:
                pinned = room_state_pinned.content["pinned"]
            except AttributeError as e:
                logging.warning(f"{e}")
                logging.warning(f"Pinning the first message in {room}")
        pinned.append(root.event_id)
        content = {"pinned": pinned}
        try:
            await client.room_put_state(room_id, "m.room.pinned_events", content)
        except Exception as e:
            logging.error(f"{e}")
            logging.warning(f"Unable to pin: {room} - {title}")
        logging.debug(f"Pinned: {room} - {title}")
        logging.debug(f"Done: {room} - {title}")
    except LocalProtocolError as e:
        logging.error(f"{e}")
        logging.error(f"{room} - {title}")
    except Exception as e:
        logging.error(f"{e}")
        logging.error(f"{room} - {title}")


async def main():
    # These are for testing:
    # from datetime import datetime, timedelta
    # from zoneinfo import ZoneInfo  # python3.8+
    # t1 = datetime.now(tz=ZoneInfo(tz)) + timedelta(seconds=5)
    # t2 = datetime.now(tz=ZoneInfo(tz)) + timedelta(seconds=6)
    # t3 = datetime.now(tz=ZoneInfo(tz)) + timedelta(seconds=7)

    # t1 = t1.isoformat()
    # t2 = t2.isoformat()
    # t3 = t3.isoformat()

    # schedule = [
    #     {
    #         "title": "First talk, room 1",
    #         "start": t1,
    #         "room": "d0207",
    #     },
    #     {
    #         "title": "First talk, room 2",
    #         "start": t1,
    #         "room": "test-matrix2",
    #     },
    #     {
    #         "title": "First talk, room 3",
    #         "start": t1,
    #         "room": "test-matrix3",
    #     },
    #     {
    #         "title": "Second talk, room 1",
    #         "start": t2,
    #         "room": "test-matrix1",
    #     },
    #     {
    #         "title": "Second talk, room 2",
    #         "start": t2,
    #         "room": "test-matrix2",
    #     },
    #     {
    #         "title": "Second talk, room 3",
    #         "start": t2,
    #         "room": "test-matrix3",
    #     },
    #     {
    #         "title": "Third talk, room 1",
    #         "start": t3,
    #         "room": "test-matrix1",
    #     },
    # ]
    schedule_file = Path("schedule_for_bot.json")
    with schedule_file.open() as f:
        schedule = json.load(f)
        logging.debug(f"Loaded schedule {schedule_file}")
    async_client = AsyncClient(url, username, device_id=device_id)
    logging.debug(f"Trying to log in as {username} on {url}")
    if password == "" and token != "":
        logging.debug("Logging in with user and token")
        response = await async_client.login(token=token, device_name=device_name)
    elif password != "" and token == "":
        logging.debug("Logging in with user and password")
        response = await async_client.login(password=password, device_name=device_name)
    else:
        logging.error("Please, provide only one between TOKEN and PASSWORD")
        return
    logging.debug("Starting to gather tasks for today...")
    await asyncio.gather(
        *(
            send_messages(
                title=talk["title"],
                room=talk["room"],
                start_time=talk["start"],
                client=async_client,
            )
            for talk in schedule
        )
    )
    await async_client.close()


if __name__ == "__main__":
    homeserver: str = getenv("HOMESERVER")  # "devconf.cz"
    url: str = getenv("HOMESERVER_URL", f"https://matrix.{homeserver}")  # "https://matrix.devconf.cz"
    username: str = getenv("USERNAME")  # "mhoyer"
    password: str = getenv("PASSWORD", "")
    token: str = getenv("TOKEN", "")
    device_id: str = getenv("DEVICE_ID")
    tz: str = getenv("TZ", "Europe/Prague")
    device_name: str = getenv("device_name", "qabot")
    replace_pinned: bool = getenv("REPLACE_PINNED", False)
    # if True, the Q&A thread is always the only pinned message

    asyncio.run(main())
