import json
from datetime import datetime
from os import getenv
from pathlib import Path
from zoneinfo import ZoneInfo  # python3.8+

import requests

api_key = getenv("SCHED_API_KEY")
event_id = getenv("SCHED_EVENT_ID", "devconfcz2023")
url = f"https://{event_id}.sched.com/api/session/export?api_key={api_key}&format=json&strip_html=Y"

response = requests.get(url, timeout=10)
event_data = response.json()

tz = "Europe/Prague"

if not event_data:
    print("No event data found")
else:
    # iterate through sessions in event data and format time fields
    auditorium_talks = []
    for session in event_data:
        start_datetime = datetime.strptime(
            f"{session['start_date']}{session['start_time']}",
            "%Y-%m-%d%H:%M:%S",
        ).replace(tzinfo=ZoneInfo(tz))
        track = [session.get("venue", "")][0]

        if "Talks" in track and session["name"] != "Welcome!":
            auditorium_talks.append(
                {
                    "title": session["name"],
                    "start": start_datetime.isoformat(),
                    "room": track.removesuffix(" | Lightning Talks")
                    .removesuffix(" | Talks")
                    .lower(),
                },
            )

    # write data to file
    path = Path("./schedule_for_bot.json")
    path.write_text(json.dumps(auditorium_talks, indent=4, ensure_ascii=False))
